
module.exports = {
  plugins: {
    autoprefixer: {
      browsers: [
        '>1%',
        'last 50 versions',
        'Firefox ESR',
        'last 10 iOS versions',
        'not ie < 9'
      ],
      flexbox: 'no-2009',
    }
  }
}
