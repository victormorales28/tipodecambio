module.exports = {
  moduleFileExtensions: [
    "ts",
    "tsx",
    "js"
  ],
  transform: {
    "^.+\\.tsx?$": "ts-jest"
  },
  testMatch: [
    "**/*.(test|spec).(ts|tsx)"
  ],
  globals: {
    "ts-jest": {
      babelConfig: true,
      tsConfig: "jest.tsconfig.json"
    }
  },
  collectCoverageFrom: [
    "src/**/*.{js,jsx,ts,tsx}",
    "pages/**/*.{js,jsx,ts,tsx}",
    "!pages/**/_*.{js,jsx,ts,tsx}",
  ],
  coveragePathIgnorePatterns: [
    "/node_modules/",
    "enzyme.js"
  ],
  setupFilesAfterEnv: ["<rootDir>/enzyme.js"],
  coverageReporters: [
    "json",
    "lcov",
    "text",
    "text-summary"
  ],
  reporters: [
    "default",
    ["jest-junit", {
      "suiteName": "jest tests",
      "outputDirectory": "coverage",
      "outputName": "junit.xml",
      "classNameTemplate": "{classname} - {title}",
      "titleTemplate": "{classname} - {title}",
      "ancestorSeparator": " > ",
      "usePathForSuiteName": "true"
    }]
  ],
  testResultsProcessor: "jest-junit",
  moduleNameMapper: {
    "\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$": "<rootDir>/__mocks__/fileMock.js",
    "\\.(css|less|scss)$": "identity-obj-proxy",
    "@Src/(.*)": "<rootDir>/src/$1",
    "@Pages/(.*)": "<rootDir>/pages/$1",
    "@Icons/(.*)": "<rootDir>/src/Icons/$1",
    "@Images/(.*)": "<rootDir>/statics/assets/img/$1",
  }
};
