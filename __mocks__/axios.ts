export default {
  get: jest.fn(() => Promise.resolve({data: {}, headers: {}})),
  post: jest.fn(() => Promise.resolve({data: {}, headers: {}})),
  put: jest.fn(() => Promise.resolve({data: {}, headers: {}})),
  delete: jest.fn(() => Promise.resolve({data: {}, headers: {}})),
  all: jest.fn(() => Promise.resolve([{data: {}, headers: {}}, {data: {}, headers: {}}]))
}
