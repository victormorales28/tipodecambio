import { FormValidationHelper, iField } from "@Src/Helpers/FormValidationHelper";

export class CalculatorModel extends FormValidationHelper {
  inpOrigin: iField = {
    value: '',
    isRequired: true,
  };

  inpDestination: iField = {
    value: '',
    isRequired: true,
  };
}
