import axios from "axios";

export class HttpPublic {
  API_KEY: string = ""

  setApiKey(API_KEY: any) {
    this.API_KEY = API_KEY
  }

  getHeaders() {
    let headers = {};
    return headers;
  }

  get(url: string, params?: any) {
    return params
      ? axios.get(url, { ...params, headers: this.getHeaders() })
      : axios.get(url, { headers: this.getHeaders() })
  }

  post(url: string, params: any) {
    return params
      ? axios.post(url, params, { headers: this.getHeaders() })
      : axios.post(url, { headers: this.getHeaders() })
  }

  put(url: string, params: any) {
    return params
      ? axios.put(url, params, { headers: this.getHeaders() })
      : axios.put(url, { headers: this.getHeaders() })
  }
}
