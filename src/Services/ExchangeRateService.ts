import axios from "axios";
import { HttpPublic } from "./HttpPublic";
import { API, ACCESS_KEY } from '@Src/Utils/Constants';

export class ExchangeRateService extends HttpPublic {
  getLastes() {
    return this.get(`${API}/latest?access_key=${ACCESS_KEY}&symbols=EUR,USD`).then(({ data }) => data);
  }

  getLastesDays(days: Array<string>) {
    return axios.all(days.map((day) => {
      return this.get(`${API}/${day}?access_key=${ACCESS_KEY}&symbols=EUR,USD`).then(({ data }) => data);
    }))
  }
}
