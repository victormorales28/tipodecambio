import { FormHelper } from './FormHelper';

export interface iField {
  value: any;
  isRequired?: boolean;
  isLoading?: boolean;
  validations?: Array<Function>;
  validity?: any;
}

export class FormValidationHelper {
  oFormHelper = new FormHelper();

  getStateInput = (form: any, event: any) => {
    let fieldName = event.target.name;
    let value = event.target.value;
    let validity = this.oFormHelper.getValidityInput(event.target.validity)
    let customError = { valid: true }

    if (event.target.customValidity) {
      validity = Object.assign({}, validity, event.target.customValidity)
    }

    if (validity.valid && form[fieldName].validations) {
      for (const validation of form[fieldName].validations) {
        if (customError.valid) {
          customError = validation(value)
        }
      }
      if (!customError.valid) {
        validity.valid = false;
      }
    }

    return {
      ...form,
      [fieldName]: {
        ...form[fieldName],
        isDirty: true,
        postSubmitError: '',
        value,
        validity,
        customError,
      },
    }
  }

  getStateSelect = (form: any, value: any, fieldName: string) => {
    return {
      ...form,
      [fieldName]: {
        ...form[fieldName],
        value,
      },
    }
  }

  setStateField = (form: any, fields: any) => {
    for (var key in fields) {
      let field = fields[key];
      field.value = typeof field.value === "undefined" ? form[key].value : field.value;
      if (form[key].value !== field.value) {
        field.isDirty = typeof field.value !== "undefined";
      }

      form[key] = Object.assign(form[key], field)
    }
    return form;
  }

  isValidForm = (refForm: any, prevForm: any) => {
    let form = { ...prevForm }
    if (refForm.current) {
      let fieldsToEvaluate = [];
      for (var key in form) {
        let field = { ...form[key] };
        if ("value" in field && (field.isRequired || field.value !== "")) {
          let refInput = refForm.current.querySelector(`[name='${key}']`)
          if (refInput) {
            if (refInput.getAttribute("data-inputtype") === "select") {
              if (refInput.value !== field.value) {
                field.validity = { valid: field.value !== "" }
              }
            }
            else {
              refInput.value = field.value
              refInput.required = field.isRequired
              field.validity = this.oFormHelper.getValidityInput(refInput.validity)
            }
            fieldsToEvaluate.push(field)
          }
          else {
            fieldsToEvaluate.push({ validity: { valid: false } })
          }
        }
      }
      return fieldsToEvaluate.every((field) => field.validity && field.validity.valid)
    }
    return false;
  }
}
