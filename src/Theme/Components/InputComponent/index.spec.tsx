import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme, { mount, shallow } from 'enzyme';

import { InputComponent } from './index';

Enzyme.configure({ adapter: new Adapter() });
URL = require("url").URL
window.dataLayer = [];

describe('<InputComponent/>', () => {
  it('render mount', () => {

    const component = mount(
      <InputComponent
        classwrapper="col-xs-12 col-sm-6 col-md-3"
        type="email"
        name="inpDocumentNumber"
        required={true}
        model={{ isDirty: true, value: '' }}
        value={'qwe@'}
        autoComplete="off"
        minLength="2"
        autoFocus={true}
        label="Número de documento*"
        onChange={() => { }}
      />
    );

    const wrapper = shallow<InputComponent>(
      <InputComponent
        classwrapper="col-xs-12 col-sm-6 col-md-3"
        type="email"
        name="inpDocumentNumber"
        required={true}
        model={{ isDirty: true, value: '' }}
        value={'qwe@'}
        autoComplete="off"
        minLength="2"
        autoFocus={true}
        label="Número de documento*"
        onChange={function () { }}
      />
    );

    const instance = wrapper.instance();

    instance.refInput = {
      current: {
        value: 'qweqwe'
      }
    }

    instance.getErrorText();

    instance.evaluateShowDropdownEmail();

    instance.hideDropdownEmail();

    instance.onSelectEmail("@gmail.com");

    expect(component.find(InputComponent)).toHaveLength(1);
  });
});
