import React, { Component } from 'react';
import './index.scss';

interface iProps {
  autoFocus?: boolean;
  classwrapper: string;
  type: string;
  name: string;
  disabled?: boolean;
  required?: boolean;
  model: any;
  value: any;
  defaultValue?: any;
  autoComplete: string;
  icon?: any;
  label: string;
  classgroup?: string;
  pattern?: any;
  minLength?: any;
  maxLength?: any;
  onChange?: Function;
}

export class InputComponent extends Component<iProps> {
  listOpntionsEmail = ["@gmail.com", "@hotmail.com", "@outlook.com", "@yahoo.com"];
  refInput: any = React.createRef()

  state = {
    showDropdownEmail: false,
    customInputType: null,
  }

  getErrorText() {
    let errorText = this.props.model.errorText || "campo";
    errorText = this.props.type === "email" ? "correo" : errorText;
    errorText = this.props.type === "tel" ? "número" : errorText;
    return errorText;
  }

  evaluateShowDropdownEmail = () => {
    if (this.props.value) {
      let showDropdownEmail = false;
      let atsLength = this.props.value.replace(/[^@]/g, "").length;

      if (this.props.type === "email" && atsLength === 1) {
        let hasCoincidence = this.listOpntionsEmail.filter((option => this.matchAtSomething(this.props.value, option)))
        showDropdownEmail = Boolean(hasCoincidence.length)
      }
      this.setState({ showDropdownEmail })
    }
  }

  hideDropdownEmail = () => {
    this.setState({ showDropdownEmail: false });
  }

  onSelectEmail = (option: any) => {
    this.refInput.current.value = `${this.props.value.replace(/@.*/g, "")}${option}`;
    this.props.onChange ? this.props.onChange({ target: this.refInput.current }) : ""
  }

  onControlPassword = () => {
    this.setState({
      customInputType: this.state.customInputType === "text" ? "password" : "text",
    })
  }

  handleChange = (event: any) => {
    if (event.target.type === "tel") {
      event.target.value = event.target.value.replace(/[^0-9]/g, "")
    }

    this.props.onChange ? this.props.onChange(event) : ""
  }

  matchAtSomething(value: any, option: any) {
    let valueAtSomething = value.includes("@") ? `@${value.replace(/.*@/g, "")}` : "";
    return valueAtSomething.length <= option.length && option.includes(valueAtSomething)
  }

  render() {
    let model = this.props.model || {}
    let value = this.props.value || this.props.defaultValue || ""
    let Icon = this.props.icon

    return (
      <div className={`c-input e-form-wrapper ${this.props.classwrapper || ""} ${this.props.icon ? "hasIcon" : ""}`}>
        {
          Icon &&
          <div className="e-form-group__icon-container"><Icon className="e-form-group__icon" /></div>
        }

        <div className={`e-form-group ${this.props.classgroup || ""} ${model.isDirty ? 'dirty' : ''} ${value.length ? 'has-value' : ''} ${model.postSubmitError ? 'invalid' : ''}`}>
          {
            this.props.type === "password" &&
            <p className="e-form-group__control-password" onClick={this.onControlPassword}>
              {this.state.customInputType === "text" ? "OCULTAR" : "MOSTRAR"}
            </p>
          }
          <input
            className="e-form-group__input e-text-medium"
            {...this.props}
            // required={this.props.required ? "required" : ""}
            required={this.props.required}
            pattern={this.props.type === "email" ? "^.+@[^\\.].*\\.[a-zA-Z]{2,}$" : this.props.pattern || null}
            ref={this.refInput}
            onKeyUp={this.evaluateShowDropdownEmail}
            onFocus={this.evaluateShowDropdownEmail}
            onBlur={this.hideDropdownEmail}
            type={this.state.customInputType || this.props.type}
            onChange={this.handleChange}
          // icon=""
          />
          <label className="e-form-group__control-label">
            {this.props.label}
          </label>
          <i className="e-form-group__bar"></i>
          <div className="e-form-group__error-messages">
            {model.validity && model.validity.tooShort &&
              <span className="e-form-group__error-message">Debe ingresar mínimo {this.props.minLength} dígitos</span>
            }
            {model.validity && model.validity.valueMissing &&
              <span className="e-form-group__error-message">Completa este campo</span>
            }
            {model.validity && model.validity.patternMismatch &&
              <span className="e-form-group__error-message">No es un {this.getErrorText()} válido</span>
            }
            {model.postSubmitError &&
              <span className="e-form-group__error-message">{model.postSubmitError}</span>
            }
          </div>

          <div className={`e-form-group__dropdown e-animation-fadein-up-select ${this.state.showDropdownEmail ? "show" : ""} `} >
            <ul className="e-form-group__dropdown-list">
              {
                this.props.type === "email" &&
                this.listOpntionsEmail.map((option) => {
                  let valueAtSomething = value.includes("@") ? `@${value.replace(/.*@/g, "")}` : "";

                  if (value !== "" && this.matchAtSomething(value, option) && valueAtSomething !== option) {
                    let textNormal = option.replace(valueAtSomething, "")
                    return (
                      <li
                        className="e-form-group__dropdown-item"
                        key={option}
                        onMouseDown={() => this.onSelectEmail(option)}
                      >
                        <span className="e-text-medium">{valueAtSomething}</span>{textNormal}
                      </li>
                    )
                  }
                  return false
                })
              }
            </ul>
          </div>

        </div>
      </div>
    )
  }
}
