const dotenv = require('dotenv').config()
const path = require('path')
const withSass = require('@zeit/next-sass')
const TsconfigPathsPlugin = require("tsconfig-paths-webpack-plugin");

const isLocal = process.env.NODE_ENV === "local";

console.log("nextconfig process.env.NODE_ENV", process.env.NODE_ENV)
console.log("nextconfig process.env.CDN_PATH", process.env.CDN_PATH)

delete dotenv.parsed["NODE_ENV"];

module.exports = withSass({
  env: dotenv.parsed,
  useFileSystemPublicRoutes: false,
  sassLoaderOptions: {
    data: `$urlBase: '${isLocal ? '' : `${process.env.CDN_PATH}`}';`,
  },
  webpack: (config, options) => {

    if (config.resolve.plugins) {
      config.resolve.plugins.push(new TsconfigPathsPlugin());
    }
    else {
      config.resolve.plugins = [new TsconfigPathsPlugin()];
    }

    if (config.mode === 'production' && Array.isArray(config.optimization.minimizer)) {
      const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
      config.optimization.minimizer.push(new OptimizeCSSAssetsPlugin({}));
    }

    config.module.rules.push({
      test: /\.svg$/,
      use: ['@svgr/webpack'],
    });

    config.module.rules.push({
      test: [/\.gif$/, /\.jpe?g$/, /\.png$/],
      loader: require.resolve('url-loader'),
      options: {
        limit: 10000,
        name: "[path][name].[hash:8].[ext]",
        publicPath: isLocal ? "http://localhost:8080" : process.env.CDN_PATH,
      },
    });

    config.resolve.alias['Src'] = path.join(__dirname, 'src')
    return config;
  },
  assetPrefix: isLocal ? "" : process.env.CDN_PATH,
  // target: "serverless",
})
