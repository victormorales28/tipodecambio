import Document, { Html, Head, Main, NextScript } from 'next/document'

class MyDocument extends Document {
  static async getInitialProps(ctx: any) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }

  render() {
    return (
      <Html lang="es">
        <Head>
          <meta name="theme-color" content="#0057C2"></meta>
        </Head>
        <body>
          <Main />
          <NextScript />
          <div id="root-float-elements"></div>
        </body>
      </Html>
    )
  }
}

export default MyDocument
