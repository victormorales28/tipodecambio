import React from 'react';
import { MemoryRouter } from 'react-router';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme, { mount, shallow } from 'enzyme';

import LandingPage from './index';

Enzyme.configure({ adapter: new Adapter() });

URL = require("url").URL

Object.defineProperty(window, 'location', {
  value: {
    href: "http://www.afpintegra.com?seccion=TeRespondemos"
  }
});

describe('<YoloFundTypePage/>', () => {
  it('render mount', () => {

    const component = mount<LandingPage>(<LandingPage />);

    const wrapper = shallow<LandingPage>(<LandingPage />);
    const instance = wrapper.instance();

    let USD = 1.1031
    instance.state.USD = USD
    instance.handleInput({ target: { type: "text", name: 'inpOrigin', value: '1000', validity: {} } })
    expect(instance.state.form.inpDestination.value).toBe("906.5361");

    instance.handleInput({ target: { type: "text", name: 'inpOrigin', value: '1000..', validity: {} } })
    expect(instance.state.form.inpDestination.value).toBe("906.5361");

    instance.handleInput({ target: { type: "text", name: 'inpOrigin', value: '050', validity: {} } })
    expect(instance.state.form.inpDestination.value).toBe("45.3268");

    instance.handleInput({ target: { type: "text", name: 'inpOrigin', value: '', validity: {} } })
    expect(instance.state.form.inpDestination.value).toBe("");

    instance.handleInput({ target: { type: "text", name: 'inpOrigin', value: '125.25', validity: {} } })
    expect(instance.state.form.inpDestination.value).toBe("113.5436");


    expect(component.find(LandingPage)).toHaveLength(1);
  });
});
