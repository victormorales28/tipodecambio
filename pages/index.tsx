import React, { Component } from 'react';
import moment from 'moment';

import "@Src/App.scss";
import './index.scss';

import { InputComponent } from '@Src/Theme/Components/InputComponent';
import { CalculatorModel } from '@Src/Models/CalculatorModel';
import { ExchangeRateService } from '@Src/Services/ExchangeRateService';

export default class LandingPage extends Component {
  oExchangeRateService = new ExchangeRateService()
  state = {
    form: new CalculatorModel(),
    USD: 0,
    EUR: 0,
    timeInterval: 600000,
    listHistoric: [],
    totalHistoricalDays: 3,
  }

  componentDidMount() {
    this.getCurrentExchangeRate()
    setInterval(() => { this.getCurrentExchangeRate() }, this.state.timeInterval)

    this.getHistoric()
  }

  getCurrentExchangeRate() {
    this.oExchangeRateService.getLastes().then((response) => {
      this.setState({
        USD: response.rates.USD.toFixed(4),
        EUR: (1 / response.rates.USD).toFixed(4),
      })
    })
  }

  getHistoric() {
    let days = []
    for (let index = 0; index < this.state.totalHistoricalDays; index++) {
      days.push(moment().subtract(index + 1, 'days').format('YYYY-MM-DD'))
    }
    this.oExchangeRateService.getLastesDays(days).then((response) => {
      this.setState({
        listHistoric: response
      })
    })
  }

  handleInput = (event: any) => {
    let prevValue = this.state.form.inpOrigin.value
    let value = event.target.value.replace(/[^0-9.]/g, "")
    if (value.split(".").length > 2) {
      value = prevValue.replace(/[^0-9.]/g, "")
    }

    let countCharacterRemove = 0
    let characterRemove = false
    for (let char of value) {
      if (!characterRemove) {
        if (char === "0" || char === ".") {
          countCharacterRemove++
        }
        else {
          characterRemove = true
        }
      }
    }
    value = value.slice(countCharacterRemove)
    let valueFormatDecimal = value.split(".")[0] + (value.split(".")[1] === undefined ? "" : "." + value.split(".")[1].slice(0, 4))

    value = this.getFormatAmount(valueFormatDecimal)
    event.target.value = value

    this.setState({
      documentFound: false,
      form: this.state.form.getStateInput(this.state.form, event)
    }, () => {
      let valueNumber = parseFloat(valueFormatDecimal)
      let valueString = this.getFormatAmount((valueNumber / this.state.USD).toFixed(4).toString())

      this.setState({
        form: this.state.form.setStateField(this.state.form, {
          inpDestination: { value: valueString !== "NaN" ? valueString : "" },
        })
      })
    })
  }

  getFormatAmount(value: string) {
    let valueAndSeparator = value.split(".")[0]
    let valueDecimal = value.split(".")[1] === undefined ? "" : "." + value.split(".")[1]
    return valueAndSeparator.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + valueDecimal
  }

  render() {
    return (
      <div className="p-landing e-container">

        <div className="e-card">
          <h3 className="p-landing__title e-h3 e-text-center">Change Euros to Dollars {this.state.EUR !== 0 ? `(${this.state.EUR})` : ""} </h3>

          <div className="p-landing__calculator">
            <div className="row">
              <div className="p-landing__calculator-simbol e-text-medium e-form-wrapper col-xs-2 col-sm-1 e-p1">
                USD
              </div>
              <InputComponent
                classwrapper="col-xs-10 col-sm-5"
                type="text"
                name="inpOrigin"
                model={this.state.form.inpOrigin}
                value={this.state.form.inpOrigin.value}
                autoComplete="off"
                maxLength="15"
                label="Quiero Comprar"
                onChange={this.handleInput}
              />

              <div className="p-landing__calculator-simbol e-text-medium e-form-wrapper col-xs-2 col-sm-1 e-p1">
                EUR
              </div>
              <InputComponent
                disabled={true}
                classwrapper="col-xs-10 col-sm-5"
                type="text"
                name="inpDestination"
                model={this.state.form.inpDestination}
                value={this.state.form.inpDestination.value}
                autoComplete="off"
                maxLength="15"
                pattern="[0-9]{0,15}"
                label="Enviaré"
              />
            </div>
          </div>

        </div>

        <div className="e-card p-landing__historic-price">
          <h3 className="p-landing__title e-h3">Historic price</h3>

          <ul className="p-landing__historic-price-list">
            {
              this.state.listHistoric.map((item: any, key) => {
                return <li className="p-landing__historic-price-item" key={key}>
                  {item.date} <span className="e-text-medium">{(1 / item.rates.USD).toFixed(4)}</span>
                </li>

              })
            }
          </ul>
        </div>
      </div>
    )
  }
}
