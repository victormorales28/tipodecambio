require('dotenv').config()
const next = require('next')
const express = require('express')
const path = require('path')
const { parse } = require('url')

const isLocal = process.env.NODE_ENV === "local";
console.log("server process.env.NODE_ENV", process.env.NODE_ENV)
console.log("server process.env.CDN_PATH", process.env.CDN_PATH)
// Next App
const nextApp = next({
  dev: isLocal,
});

// Next Handle
const nextHandle = nextApp.getRequestHandler();

nextApp.prepare().then(() => {
  // Express App
  const app = express();

  if (isLocal) {
    // server - folder
    app.use('/statics/assets/fonts', express.static('statics/assets/fonts'))
    app.use('/statics/downloadable', express.static('statics/downloadable'))
    app.use('/statics', express.static('.next/statics'))
  }
  else {
    nextApp.setAssetPrefix(process.env.CDN_PATH)
  }

  app.use('/', express.static('public'))

  app.get('/', (req, res) => {
    const parsedUrl = parse(req.url, true)
    const { query } = parsedUrl
    nextApp.render(req, res, '/', query)
  });

  if (isLocal) {
    app.get('*', (req, res) => nextHandle(req, res));
  }
  else {
    app.get('*', (req, res) => {
      res.sendFile(path.join(__dirname, './html/index.html'));
    });
  }

  app.listen(8080, err => {
    if (err) {
      throw err;
    }

    console.log('Running server on http://localhost:8080');
  });
}).catch(err => {
  console.error(err.stack);
  process.exit(1);
});
