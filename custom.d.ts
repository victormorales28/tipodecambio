declare module "*.svg" {
  const content: any;
  export default content;
}

declare module "*.jpg" {
  const content: any;
  export default content;
}

declare module "*.jpeg" {
  const content: any;
  export default content;
}

declare module "*.png" {
  const content: any;
  export default content;
}

declare module "*.gif" {
  const content: any;
  export default content;
}

declare module "*.ico" {
  const content: any;
  export default content;
}

declare module "*.webp" {
  const content: any;
  export default content;
}

declare var gaSendEvent: any;
declare var dataLayer: any;
declare var document: any;
declare var grecaptcha: any;
declare var fbq: any;
declare var gtag_report_conversion: any;

declare global {
  interface Window { grecaptcha: any; }
}
window.grecaptcha = window.grecaptcha || {};

// Mock Function
declare module "foo" {
  interface FooContext {
    bar: number;
  }
  export function useFooContext(): FooContext;
}
// Mock Function
